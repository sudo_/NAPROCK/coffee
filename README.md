[![Open Source Love](https://badges.frapsoft.com/os/v2/open-source.svg?v=103)](https://github.com/TeamSudoCoders/Coffee)
# TSUNAI
Codename: Coffee ☕ Also known as: TsunA.I, tsunA.I
Code authored by: James and Bryan from s u d o\_, and various other sources, including pjreddie (darknet).


## Foreword
Great to see you here! This is our repository that hosts the code available for TSUNAI, the project submission for NAPROCK 2018!
We've worked countless of manhours for this project to come to a reality, so we hope you will like it!


## Introduction
TSUNA.I. is a computing solution that combines Artificial Intelligence (AI), Blockchain, Cloud Computing, IoT, and Machine Vision (MV) to build an ecosystem targeted towards
fishing industries and consumers, in turn transform tuna into a sustainable protein source worldwide.


TSUNA.I. consist of 3 subsystem: Species Identification, Quality Prediction, and Authentic Sourcing 


The Species Identification is used to store the amount of each tuna species is caught.  Through these large amounts of data, we can gather insights on migration pattern and/or growth pattern of tuna and in turn foster a sustainable marine ecosystem and food source.


Quality Control alerts users the quality of tuna being caught in that specific region/area/environment. Using the cross section of the tuna’s tail, we predict whether it is chutoro, otoro or akami, assuming that the more red meat the tuna has, the less valuable it is.


Authentic Sourcing allows a user to verify that the tuna is caught through TSUNA.I. using blockchain technology. In the long run we believe TSUNA.I. will allow a steady supply of tuna that are of high quality while still maintaining the biodiversity and survival of tuna and oceanic creatures alike.


**Disclaimer**: Due to the complexity of the system, the software setup inevitably becomes complicated. Please be prepared to spend an entire afternoon to setup the whole system!


## Prerequisites
- An [Amazon Web Services](https://aws.amazon.com) account with access to EC2.
- Operative Linux Experience (Specifically, the operator must be familiar with the distro Ubuntu 16.04 LTS)
- Personal Computer (Training can also be done on AWS EC2 to speed up training).
  - Ubuntu 16.04, amd64 architecture
  - NVIDIA Dedicated Graphics Processing Unit (GPU), with at least 4GB of Video Random Access Memory (VRAM)
  - Processor that is better or equivalent to i5-3470
  - 8GB or more of DDR3 Random Access Memory (RAM)
- Mozilla Firefox. Both on the operating computer, and the client. Some websites refuses to work without Firefox, due to the CORS-ORIGIN issues.


## Setting up
This section explains how to setup a complete TSUNA.I. system.


We assumed that the notation "./" is the directory where users cloned the repository into, and will be what we refer to from here on out.  Please "cd" into the cloned repository as often as possible throughout the setup phase.


Any text in angle brackets, i.e. "<>", are replaceable text, and should be replaced with a string best matching the description within the angle brackets, i.e. "<insert your most important password here>" should be replaced with "hunter2" (note the quotes).


Run the following commands to get all packages that are already currently on your system up-to-date:
```
sudo apt update && sudo apt full-upgrade -y
```


### Basic tools setup
Run these commands to get most of the tools required:
```
wget https://dl.google.com/go/go1.11.1.linux-amd64.tar.gz && wget https://github.com/git-lfs/git-lfs/releases/download/v2.5.2/git-lfs-linux-amd64-v2.5.2.tar.gz \
sudo apt install git gcc g++ make nodejs npm \
sudo tar -C /usr/local/ -xvf go1.11.1.linux-amd64.tar.gz \
mkdir lfs && tar -C lfs -xvf git-lfs-linux-amd64-v2.5.2.tar.gz && sudo lfs/install.sh \
echo export GOROOT=/usr/local/go >> .profile \
echo export GOPATH=\$HOME/go >> .profile \
echo export PATH=\$GOPATH/bin:\$GOROOT/bin:\$PATH >> .profile \
source ~/.profile \
git lfs install
```

Verify your packages:
```
git --version && gcc -v && g++ -v && go version && make -v
```

Clone this Git (LFS) repository:
```
git lfs clone https://github.com/TeamSudoCoders/Coffee
```

Run the following commands to verify that darkent is working:
```
cd darknet \
./darknet detector test cfg/coco.data cfg/yolov2.cfg data/dog.jpg
```


### Darknet - YoloV2
Follow [this](https://yangcha.github.io/CUDA90/) guide to install Cuda 9.0.176 and cuDNN 7.
Follow **point 2.1** in [this](https://milq.github.io/install-opencv-ubuntu-debian/) link to install OpenCV 3.4.2.


Some pre-trained weights are included in the repository (you should have acquired it via `git lfs clone` earlier). These weights are trained on a system with the following specs:
- CPU: i7-8750H
- RAM: 16GB DDR4 RAM
- GPU: NVIDIA GTX 1050 4GB


Tuna Species weights (found in `./yolov2_tuna_species`):
- `yolov2_tunaspecies_1000.weights`
- `yolov2_tunaspecies_8000.weights`
- `yolov2_tunaspecies_10000.weights`
- `yolov2_tunaspecies_30000.weights`


Tuna Cross Section weights (found in `./yolov2_cross_section`):
- `yolov2_tunacrosssection_6000.weights`
- `yolov2_tunacrosssection_8000.weights`
- `yolov2_tunacrosssection_10000.weights`
- `yolov2_tunacrosssection_20000.weights`
- `yolov2_tunacrosssection_30000.weights`

#### Dataset
Links to the datasets used to train the model are provided below. It is highly recommended for the operator to download and replicate the results. Labelling the images is known to take a few hours.

**Disclaimer**: None of these images are owned by s u d o\_.
**Disclaimer**: More data would provide a more accurate model. We do not have sufficient data points to represent the population as a whole.

- [Dataset for Tuna Species](https://drive.google.com/drive/folders/13wmC_Wmg2Vgzmh1HcNvYMS_qPvQeNstl?usp=sharing)
- [Dataset for Cross Section](https://drive.google.com/open?id=1COp0Ye1k95qaoK3TEMJ_G_Mu4EPZYu_B)

#### Methodology
The dataset of tuna species and tuna cross section are split to a train-test ratio of 10 : 1 and 8 : 2 respectively. The max iterations is set to 400,000. Deciding when the training should stop is important as training it for excessively long may result in an increase of false positives and/or false negatives, while training just enough to minimize the number of true positives and true negatives.


To validate the test set and training set, observe the Intersection over Union (IOU), Precision, Recall, and mAP (mean Average Precision) of each weights. The definition of each terms can be summarized as follows:
![yay it's not loading, maybe refresh?](images/methodology_definitions.png)


Whereby:
	Precision = True Positive/(True Positive + False Positive)
	Recall = True Positive/(True Positive + False Negative)


A good balance between Recall and Precision is required on the test set. 


To find these values, copy the `2007_valid.txt` content of either category (tuna species/cross section) and paste it to home/username/darknet/data/valid.txt. Then run the following command: 
```
darknet recall <datafile/path>.data yolov2_<choose a cfg file>.cfg <yolov2/weights/path>.weights
```


Since this is a multiclass prediction, the mean value for Average Precision for each class is used as model evaluation. The average value of 11 points on PR-curve for each possible classification threshold (each probability of detection) for the same class is to be used to find mAP. These values are deterministic of which weights are best fit, i.e. neither underfit nor overfit (refer to the graph above).


In this dataset, the difference between all the trained weights were small, so the one with the highest Intersection over Union (IOU) was used for the model.


Weights of 10,000 or 8,000 iterations for `tuna_species` has yielded acceptable results, while for tuna cross section we chose weights of 30,000.


**Disclaimer**: More data would provide a more accurate model. We do not have large amounts of data.

By the end of all this section, developers should have the following items ready:
- darknet compiled with CUDA, CuDNN and OpenCV enabled
- Configuration files of yolov2 for tuna species and cross section
- Weights for tuna species and cross section
- darknet convolutional layers (optional, but increases training speed) 


Finally to use any of the result, below is the format of running yolov2
```
./darknet detector test <datafile>.data <yolov2>.cfg <yolov2_results>.weights
```
Enter your image path to do a prediction. Optionally, change test to demo for live streaming.
For further development of A.I, please read the original commit [here](https://github.com/pjreddie/darknet).


### Amazon Web Serevices (AWS)
All code deployable to AWS is also deployable to local servers. AWS serves as the platform for scaling and and elasticity to meet computing load, especially when the load varies between light blockchain manipulation to real-time simultaneous multi-device camera streaming to the backend servers.

![the architecture's not loading! maybe reload?](images/aws_architecture.png)

It is required that the operator has cloned the repository on their local machines.


The folder cloned to (usually "./coffee") will be referred to as "./", meaning that operators should perform cd coffee as frequently as possible for the commands to work as intended.


#### Web Presence 1 (./WP1) - Basic (Check README.md in ./WP1 for  advanced section)
1. Web Presence 1 serves as front end for the detection of tuna species. The required folders are: ./WP1 and ./darknet. The following step outlines the steps required to deploy it on AWS:
2. Log on to the AWS Dashboard
3. Select Services > EC2
4. Click Launch Instance
5. Find the Ubuntu Server 16.04 LTS `(ami-059eeca93cf09eebd)` image.
6. Lauch `p3.16xlarge`, this is to minimize difference in setup. We use `t2.micro` for demonstration purposes
7. Click on Review and Launch, and click on Launch after reviewing the details
8. When prompted for a keypair, please choose to either create one, or use an existing one that you still have private key access to, then launch the instance.
9. Give the instance some time to warm-up, which can take several minutes. Meanwhile, increase the security of your key file through this command:
    ```
    chmod 700 "<path to keypath>"
    ```
10. SSH into your instance, by using the key you have downloaded, and the public IP assigned to the instance (found under the IPv4 Public IP column) through the use of the command: 
    ```
    ssh -i "<path to key>" "<ec2 public ip>"
    ```
12. Refer to the "Basic Tools" section to install the necessary tools on the EC2 instance.
13. Launch a separate terminal instance from the instance running SSH.
14. Perform these commands to copy ./WP1 and ./darknet from your local machine to the EC2 instance:
    ```
    scp -r -i "<path to key>" ./WP1 ubuntu@<ec2 public ip>:/home/ubuntu/ \
    scp -r -i "<path to key>" ./darknet ubuntu@<ec2 public ip>:/home/ubuntu/
    ```
15. Decide on and install required darknet dependencies. We recommended for production and GPU instances to refer "Darknet - Yolo V2" section to install cuda, cuDNN, and OpenCV.
16. Generate the certificates, filling up the fields as necessary. Note that the field where they ask for the canonical name should be filled with the public ip of the EC2 instance.
    ```
    cd ./WP1/
    openssl genrsa -out server.key 2048 \
    openssl ecparam -genkey -name secp384r1 -out server.key \
    openssl req -new -x509 -sha256 -key server.key -out server.crt -days 365
    cd ../
    ```
17. Install the Go dependency required and run the server
    ```
    go get github.com/gorilla/websocket \
    cd ./WP1/ \
    go run server.go
    ```
18. Back in the AWS Dashboard, select your instance, and find Security Groups under the description tab.
19. If you launched your instance without configuring anything extra, there will only be one security group present, with the prefix "launch-wizard-". Click onto the security group.
20. On the bottom of the screen, click on the Inbound tab.
21. Click Edit, and add a Custom TCP type rule, and set the port range to 8080, with the source being 0.0.0.0/0.
22. Click Save.
23. Test your handiwork by visiting the IP address at: `https://<ec2 public ip>:8080`
24. You should see a security warning.
25. Click on Advanced, and Add Exception…. On the popup dialog, click Confirm Security Exception.
26. Allow camera access when prompted, and the predictions should start streaming in. (Figure on the right: The web presence)

### Web Presence 2 (./WP2)
Web Presence 2 serves as the tuna quality detection, alongside publishing contracts to the blockchain. Due to the constraints of time, the Web Presence is not fully assembled, and will have an updated manual in `./WP2/README.md` once complete.

### Web Presence 3 (./WP3)
Web Presence 3 serves as the consumer validator for tuna. As Web Presence 3 communicates with a local blockchain member node, it means that each instance created will need to be a member node in the Ethereum network. This also implies that the **bootnode should already be running** and you should have the **enode address** and **initial account id** before you can setup Web Presence 3.
1. Follow steps 1 to 12 of setting up Web Presence 1, but choose a t2.micro instance instead.
2. Install docker.io and solidity into the EC2 instance:
    ```
    sudo add-apt-repository ppa:ethereum/ethereum \
    sudo apt-get update \
    sudo apt install docker.io solc \
    sudo usermod -G docker ubuntu \
    newgrp docker
    ```
3. Perform these commands in your local computer to copy ./WP3 and ./blockchain from your local machine to the EC2 instance:
    ````
    scp -r -i <path to key> ./WP3 ubuntu@<ec2 public ip>:/home/ubuntu/ \
    scp -r -i <path to key> ./blockchain ubuntu@<ec2 public ip>:/home/ubuntu/
    ```
4. Back in the EC2 instance, compile the contract:
    ```
    cd ./blockchain/ && echo var tuna_import=$(solc --optimize --combined-json abi,bin tuna.sol) > ./Member/tuna.js && cd ../ \
    cp ./blockchain/Member/tuna.js ./WP3/tuna.js
    ```
5. Back in the EC2 instance, build the docker for the member node:
    ```
    cd ./blockchain/Member/ \
    docker build . -t tsunai_member \
    cd ../../
    ```
6. Run the member node container:
    ```
    docker run -P -p 0.0.0.0:8082:8082 -it tsunai_member <enode address> <initial account id>
    ```
7. You will be prompted for a password thrice. The first set prompt (with the "Repeat Passphrase" prompt) is for the creation of the password, and the last prompt is for the client to unlock your account. (Note: 3 times)
8. Open another terminal, and SSH into the EC2 instance.
9. Find out and note down the docker container name:
    ```
    docker container ls
    ```
10. Transfer over the contents of ./WP3 into the docker:
    ```
    docker exec <docker container name> mkdir /server \
    docker cp ./WP3/. <docker container name>:/server
    ```
11. Install dependencies in the docker:
    ```
    docker exec <docker container name> apk add nodejs npm python gcc g++ git make \
    docker exec <docker container name> /bin/bash -c "cd /server/ && npm install"
    ```
12. Run WP3:
    ```
    docker exec <docker container name> /bin/bash -c "cd /server/ && node main.js"
    ```
13. Do steps 18 to 22 under the section of "Web Presence 1", however, instead of allowing inbound port 8080, use the port 8082 instead.
14. Check the result of your handiwork: `http://<ec2 ip>:8082/`
15. Get an existing retailer ID (this is the account address to verify), fish ID (contract address), and fish part (A to G, based on the diagram shown in the same page), and test out the page. It may be necessary to click the submit button multiple times to get a result.
16. The result will indicate if the tuna part is genuinely bought by the retailer. To process another result, you will need to refresh the page.

### Bootnode
The first ethereum node to be run is known as the bootnode. This is the authority in the Proof of Authority (PoA) style blockchain.
1. Perform steps 1 and  2 as stated in the section, "Web Presence 3", choosing a larger instance like `t3.2xlarge`.
2. Perform the following command on your computer to transfer the necessary folder to the EC2 instance:
    ```
    scp -r -i <path to key> ./blockchain ubuntu@<ec2 public ip>:/home/ubuntu/
    ```
3. Compile the contract on EC2 instance:
    ```
    cd ./blockchain/ && echo var tuna_import=$(solc --optimize --combined-json abi,bin tuna.sol) > ./Bootnode/tuna.js && cd ../
    ```
4. Build Dockerfile on EC2 instance:
    ```
    cd ./blockchain/Bootnode && docker build . -t tsunai_bootnode && cd../..
    ```
5. Start the bootnode:
    ```
    docker run -it -p 0.0.0.0:30301:30301 -p 0.0.0.0:30301:30301/udp -p 0.0.0.0:30303:30303 -p 0.0.0.0:30303/udp tsunai_bootnode
    ```
6. You will be prompted to create an account. Please also note the **enode address** and **initial account number**. Your **enode address** will contain an IP within the string, such as `enode://<not important>@172.17.0.2:30301`. Please replace `172.17.0.2:30301` with `<ec2 public ip>:30303` when noting it down, otherwise, member nodes will not be able to connect.

7. Type `mine` and press enter. You will be prompted for your password.
8. Open another terminal, and ssh into the EC2 instance.
9. Run the command and note the name of the running container inside the EC2 instance:
    ```
    docker container ls
    ```
10. Copy the compiled contract to the container from the EC2 instance:
    ```
    docker exec -it <container name> /bin/bash -c "cd /geth && geth attach data/geth.ipc"
    ```
11. Type the following script into the console to deploy a contract onto the blockchain:
    ```js
    loadScript("tuna.js")
    var tuna_abi = tuna_import.contracts["tuna.sol:Tuna_Sale"].abi
    var tuna_bin = "0x" + tuna_import.contracts["tuna.sol:Tuna_Sale"].bin
    var tuna_contract = eth.contract(JSON.parse(tuna_abi))
    var tuna_instance = tuna_contract.new(eth.accounts[0], 0, {from: eth.accounts[0], data: tuna_bin, gas: 5000000})
    var tuna_addr = eth.getTransactionReceipt(tuna_instance.transactionHash).contractAddress
    var tuna = tuna_contract.at(tuna_addr)
    ```
13. Keep the console open, just in case you need to interact with the bootnode to send transactions to other accounts (like a member node’s account, for example)
14. Type in `tuna_addr` to obtain the address of the tuna. Note this down somewhere. This also represents the tuna ID.
15. Do steps 18 to 22 under the section of "Web Presence 1", however, instead of allowing inbound port 8080, allow 30301 and 30303 for **both TCP and UDP**.

### Local Machine
#### Merchant
The Merchant’s page allow users to transact their tunas and tuna parts to another account. This is hosted locally, as the merchant’s account and ether is accessed directly:
1. Install required dependencies (also install the tools in [basic setup](#Basic tools setup) if not done already)
    ```
    sudo add-apt-repository ppa:ethereum/ethereum \
    sudo apt-get update \
    sudo apt install docker.io solc \
    sudo usermod -G docker ubuntu \
    newgrp docker
    ```
2. Compile the contract
    ```
    cd ./blockchain/ && echo var tuna_import=$(solc --optimize --combined-json abi,bin tuna.sol) > ./Member/tuna.js && cd ../
    ```
3. Build Dockerfile:
    ```
    cd ./blockchain/Member && docker build . -t tsunai_member && cd ../..
    ```
4. Run the docker:
    ```
    docker run -P -p 0.0.0.0:8080:8080 -p 0.0.0.0:30303:30303 -it tsunai_member <enode address> <initial account id>
    ```
5. You will be asked to type your password three times.
6. Open another terminal, and run the following command to note the container name:
    ```
    docker container ls
    ```
7. Copy the necessary files over to the docker:
    ```
    docker exec <container name> mkdir /server \
    docker cp ./blockchain/Member/merchantServer.js <container name>:/server/merchantServer.js \
    docker cp ./blockchain/Member/views/ <container name>:/server/ \
    docker cp ./blockchain/Member/tuna.js <container name>:/server/tuna.js \
    docker cp ./blockchain/Member/package.json <container name>:/server/package.json
    ```
8. Install required dependencies in the container:
    ```
    docker exec <container name> /bin/bash -c "apk add gcc g++ nodejs npm git make python && cd /server && npm install"
    ```
9. Run the application:
    ```
    docker exec <container name> /bin/bash -c "cd /server && node merchantServer.js"\
    ```
10. Check your handiwork through: `http://localhost:8080`
11. If you still have a bootnode console open, you can add funds to your account by transferring from `eth.account[0]` to the account number you can acquire from `http://localhost:8080`. Use:
    ```
    eth.sendTransaction({from: eth.account[0], to: "<account num here>", value: web3.toWei(5000, "ether")})
    ```
12. Try to transact all the fish parts from the bootnode to the account number acquired in Step 10, by running this in the bootnode:
    ```
    tuna.sellAllParts.sendTransaction("<account num here>", {from: eth.accounts[0], gas: 5000000})
    ```
13. Key in the fish ID (acquired from the bootnode as a contract ID), and see the results.
14. A to G represents the fish parts, and only the fish parts present will be displayed on the web presence. The text field is the destination address (sell to) that the fish part should go to.


## LICENSE
This repository is unlicensed currently, not because we are putting the code on the public domain, but because the NAPROCK Organizing Committee automatically owns the code. They will impose their own license after the competition is over.
 

