//Express-based app to:
//1) Serve files required to display a tangible web page
//2) Communicate with the JSON RPC API
var express = require('express');
var app = express();

const fs = require('fs')
const Web3 = require('web3')
const web3 = new Web3("http://localhost:8545")

// Load the tuna contract
var content = JSON.parse(fs.readFileSync("tuna.js", {encoding: "utf-8"}).split('=')[1]);
const abi = content.contracts["tuna.sol:Tuna_Sale"].abi;
const contract = new web3.eth.Contract(JSON.parse(abi));

// Basic all allow file server
app.get('/:name', (req, res) => {
    var options = {
	root: __dirname,
	headers: {
	    'x-timestamp': Date.now(),
	    'x-sent': true
	}
    };

    var fileName = req.params.name;
    res.sendFile(fileName, options, (err) => {
	if (err) {
	    console.log("Error while sending a file: ", err);
	    res.sendStatus(500);
	} else {
	    console.log(req.ip, ": ", fileName, " sent.");
	}
    });
});

//Render the index page
app.get('/', (req, res) => {
    console.log(req.query)
    // Step 1, no queries
    if (Object.keys(req.query).length === 0) {
	web3.eth.getAccounts().then((accounts) => {
	    res.render('index.ejs', {account: accounts[0]}, (err, html) => {
		if (err) {
		    console.log("Error rendering the index file: ", err);
		    res.sendStatus(500);
		} else {
		    res.send(html);
		}
	    });
	});
	return
    }

    // Step 2, with fish id query only
    if ("fishid" in req.query && !("0" in req.query || "1" in req.query || "2" in req.query || "3" in req.query || "4" in req.query || "5" in req.query || "6" in req.query)) { //yes I'm aware that you can use a for loop here. I'm lazy. Do it yourself.
	var current_contract = contract.clone();
	current_contract.options.address = req.query.fishid; //sets the address

	web3.eth.getAccounts().then((accounts) => { //obtain what fish part the user owns
	    const acc = accounts[0];
	    var arr = new Array(7); //create array of 7 objs
	    var promises = new Array(7); //create promises of 7

	    (async function loop() {
		for (var i = 0; i < 7; i++) { //create many promises
		    await current_contract.methods.getTunaHolder(i).call((err, result) => {
			if (err) {
			    console.log("Error while getting the tuna holders from blockchain: ", err);
			    res.sendStatus(500);
			} else {
			    arr[i] = result[0]; //address of the owner of the tuna part
			}
		    }); 
		}
	    }) ().then(() => { //wait for all the promises to be done
		res.render("fishid.ejs", {account: acc, array: arr, fishID: req.query.fishid}, (err, html) => {
		    if (err) {
			console.log("Error while rendering html for fish id stage: ", err);
			res.sendStatus(500);
		    } else {
			res.send(html); //sends the rendered html
		    }
		})
	    })
	})
    }

    // Step 3: Actually do the transaction
    if ("fishid" in req.query && ("0" in req.query || "1" in req.query || "2" in req.query || "3" in req.query || "4" in req.query || "5" in req.query || "6" in req.query) && "toAccount" in req.query) {
	// Gets the tuna parts in a computationally friendly form
	var requests = new Array(0);
	for (var i = 0; i < 7; i++)
	    if ("" + i in req.query)
		requests.push(i);

	// Duplicates the contract
	var current_contract = contract.clone();
	current_contract.options.address = req.query.fishid;

	// Spawns multiple transaction requests, then brings the user back to the first page
	web3.eth.getAccounts().then((accounts) => {
	    (async function() {
		console.log(accounts)
		for (var i = 0; i < requests.length; i++) {
		    await current_contract.methods.sellPart(req.query.toAccount, requests[i]).send({from: accounts[0], gas: 5000000});
		}
	    }) ().then(() => {
		res.redirect('/');
	    });
	});
    }
});

//Starts the web app
app.listen(8080);
