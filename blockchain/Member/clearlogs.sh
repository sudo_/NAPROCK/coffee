#!/bin/bash
# Deletes logs (geth.log) every 1 hour.
while true; do
	sleep 3600 
	if [ -a "geth.log" ]; then
		cat /dev/null > miner.log
	fi
done
