#!/bin/bash
set -e

if [ "$1" = "" ]; then
	/bin/bash
else
	if [ "$2" = "" ]; then
		echo "You supplied insufficient arguments."
		/bin/bash
	else
		# Find and replace account number
		sed -r "s/ffffffffffffffffffffffffffffffffffffffff/$2/g" genesis.json -i
		# Initialize geth
		geth --datadir=data init genesis.json
		# Prompts the user to create an account
		geth --datadir=data account new 2>/dev/null
		accnum=$(geth --datadir=data account list 2>/dev/null | sed -r "s/.*\{(.*)\}.*/\1/g")
		# Start geth (hogs the terminal)
		geth --datadir=data --unlock $accnum --rpc --bootnodes=$1 --gasprice=1
	fi
fi
