# apiserver.py - Accepts POST requests to parse and submit jobs to the batch contractor
# Author: James for s u d o _

import boto3
import http.server
import json
import json_parser.parser
import batch_contractor.batch_contractor
import time
import threading
import ssl

from urllib import parse
from smtplib import SMTP_SSL

PORT = 6001
dynamodb = boto3.client('dynamodb')
TABLENAME = "tsunai_db"

## SMTP configuration
SMTPSERVER = input("Please enter SMTP Server (must be SSL'd): ")
EMAILADDR = input("Please enter email: ")
EMAILPASS = input("Please enter the email password: ")

class POSTServer(http.server.BaseHTTPRequestHandler):
    def do_POST(self):
        # Read JSON data from rfile
        length = int(self.headers['content-length'])
        data_raw = self.rfile.read(length)
        data_raw = parse.parse_qs(data_raw) # do blasphemus "magic" to "raw data"

        if b"data" in data_raw:
            data_raw[b"data"] = parse.unquote_plus(data_raw[b"data"][0].decode())

        try:
            # Uses parser to obtain a more suitable input
            data = json_parser.parser.parser(json.loads(data_raw[b"data"]))

        except json.JSONDecodeError as e:
            print("Error occured while decoding the JSON: " + e.msg)
            self.send_error(400, message="Invalid JSON Request.")
            return

        self.send_response(200, message="Submitted.")
        self.end_headers()

        def func(): # function used for threading, so that the headers return ASAP
            # Proces the request, getting the contract addresses in return
            ca = batch_contractor.batch_contractor.batch_contracting(data)
            print(str(ca))

            # Sends the raw JSON request to DynamoDB
            dynamodb.put_item(TableName=TABLENAME, Item={
                'id': {
                    'S': str(int(round(time.time() * 1000))) # TODO: Use secrets to generate a unique hash everytime
                },
                'data': {
                    'S': str(data_raw)
                }
            })
            
            # Sends an email to the requester
            with SMTP_SSL(SMTPSERVER) as smtp:
                smtp.login(EMAILADDR, EMAILPASS)
                smtp.sendmail(EMAILADDR, data_raw[b"email"][0].decode(), ("From: %s\r\nTo:%s\r\nSubject:%s\r\n\n%s" % (EMAILADDR, data_raw[b"email"][0].decode(), "Processed Tuna Batch", "These are your fish IDs: " + str(ca)))) # sends the contract addresses to the email
            
            return

        t = threading.Thread(target=func)
        t.start() # Dangling thread, but it'll probably return in time.
        return

if __name__ == "__main__":
    httpd = http.server.HTTPServer(("", PORT), POSTServer)
    httpd.socket = ssl.wrap_socket(httpd.socket, keyfile="server.key", certfile="server.crt", server_side=True, ssl_version=ssl.PROTOCOL_TLSv1)
    httpd.serve_forever()
