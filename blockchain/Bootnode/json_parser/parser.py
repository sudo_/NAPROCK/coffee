# parser.py - Parses the JSON that comes as a result of WP2.
# Written by Bryan for s u d o

def parser(jsonObj):
    fishCounts = {
        "bigeye_tuna":0,
        "bluefin_tuna":0,
        "skipjack_tuna":0,
        "yellowfin_tuna":0
    }
    for predictionRes in jsonObj["data"]:
        temp = predictionRes
        temp = temp.split(':')
        #print(temp)
        key = temp[0]
        #print(key)
        fishCounts[key] += 1
    
    newFishCount = {
        "0" : fishCounts["bigeye_tuna"],
        "1" : fishCounts["bluefin_tuna"],
        "2" : fishCounts["skipjack_tuna"],
        "3" : fishCounts["yellowfin_tuna"]
    }
    return newFishCount
