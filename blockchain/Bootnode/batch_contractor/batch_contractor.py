# batch_contractor.py - Processes contracts in batches
from web3 import Web3
from web3.middleware import geth_poa_middleware # geth style poa support
import json
ipc_provider = Web3.IPCProvider('data/geth.ipc') # use ipc provider (script will be on bootnode)

# Contract setup (returns a contract object)
def setup_contract(w3, compiled_json):
    usable_json = compiled_json.split('=')[1].strip() # compilation usually outputs with var tuna_import = ...
    obj_json = json.loads(usable_json)
    c_abi = obj_json["contracts"]["tuna.sol:Tuna_Sale"]["abi"]
    c_bin = "0x" + obj_json["contracts"]["tuna.sol:Tuna_Sale"]["bin"]

    return w3.eth.contract(abi = c_abi, bytecode = c_bin)

# Function to asynchrnously run a job to add each tuna into the blockchain
def batch_contracting(tunas):
    w3 = Web3(ipc_provider) # starts a initialize Web3 instance
    w3.middleware_stack.inject(geth_poa_middleware, layer=0) # adds the poa middleware

    contract = setup_contract(w3, open("tuna.js").read()) # sets up the contract
    contractAddresses = list() # list of contract addresses

    print("Using the account: {:s}".format(w3.eth.accounts[0]))
    
    # Add fishes onto the blockchain
    for k in tunas: # k is a stringified number
        for i in range(tunas[k]):
            account = w3.eth.accounts[0]
            tx_hash = contract.constructor(account, int(k)).transact({'from': account, 'gas': 5000000})
            tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)

            contractAddresses.append(tx_receipt.contractAddress)

    # Done
    return contractAddresses
