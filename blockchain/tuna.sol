pragma solidity ^0.4.25;

// @title Contract for the exchange of tuna from the sea
contract Tuna_Sale {
	// The available species.
	enum Tuna_Species {
		BigEye, BlueFin, SkipJack, YellowFin
	}

	// The labelled parts of the tuna. Append ", <alphabet>" before ",size". 
	enum Tuna_Parts {
		A, B, C, D, E, F, G ,size
	}

	// A structure used to store data on the different parts of the fish.
	struct Tuna_State {
		address[7] holders; //the current owners of the fish
		bool[7] done; //current owners of the fish can mark a part as "done". A "done" part cannot take part in transactions.
		uint256 blockNo; //the block number that this tuna_state was finalized
	}

	Tuna_State[] history; //contains all the tuna states since origin. The authors do not envision history containing too many elements, because 'history' will stop once it reaches consumers.
	Tuna_State current; //contains the current tuna state. Analogous to the last element in the history array.

	address public origin; //the fishermen/company that caught the tuna (set to owner if does not exist)
	address public owner; //the owner of the contract

	Tuna_Species species; //the species of the tuna

	// A modifier that only allows holders of the fish to interact with certain parts of the contract
	modifier onlyHolder {
		require (msg.sender == current.holders[uint(Tuna_Parts.A)] ||
				msg.sender == current.holders[uint(Tuna_Parts.B)] ||
				msg.sender == current.holders[uint(Tuna_Parts.C)] ||
				msg.sender == current.holders[uint(Tuna_Parts.D)] ||
				msg.sender == current.holders[uint(Tuna_Parts.E)] ||
				msg.sender == current.holders[uint(Tuna_Parts.F)] ||
				msg.sender == current.holders[uint(Tuna_Parts.G)], "Only holders of fish parts can call this function.");
		_;
	}

	// On contract creation, assign all variables to rightful values
	constructor(address _origin, Tuna_Species _species) public {
		owner = msg.sender;
		origin = _origin;

		// Initialize the tuna state
		for (uint i = 0; i < uint(Tuna_Parts.size); i++) { //assign all holders to owner
			current.holders[i] = owner;
			current.done[i] = false;
		}
		current.blockNo = block.number;
		history.push(current); //reference, storage -> storage

		species = _species;
	}

	// Sell tuna (part)
	function sellPart(address to, Tuna_Parts part) onlyHolder public {
		require (msg.sender == current.holders[uint(part)], "You do not own that part of the fish.");
		require (!current.done[uint(part)]); //fish cannot be done

		Tuna_State memory state = current; //creates a copy of current
		history.push(state); //copies, memory -> state
		history[history.length - 1].holders[uint(part)] = to; //set the new part owner to the 'to' address
		history[history.length - 1].blockNo = block.number; //set the current block number
		current = history[history.length - 1]; //copy, state -> state
	}

	// Sell tuna (all owned)
	function sellAllParts(address to) onlyHolder public {
		Tuna_State memory state = current; //copies, storage -> memory
		history.push(state); //copies, memory -> storage

		for (uint i = 0; i < uint(Tuna_Parts.size); i++) { //reassign everything owned by the message sender
			if (history[history.length - 1].holders[i] == msg.sender && !history[history.length - 1].done[i]) {
				history[history.length - 1].holders[i] = to;
			}
		}
		history[history.length - 1].blockNo = block.number; //set the current block number
		current = history[history.length - 1]; //copy, state -> state
	}

	// Function to get the tuna holder of a specific part
	function getTunaHolder(Tuna_Parts part) public view returns (address, uint256) {
		return (current.holders[uint(part)], current.blockNo);
	}

	// Function to get the past tuna holders of a specific part
	function getTunaHolderHistory(Tuna_Parts part) public view returns (address[], uint256[]) {
		address[] memory list = new address[](history.length);
		uint256[] memory bl_h = new uint256[](history.length);

		for (uint i = 0; i < history.length; i++) {
			list[i] = history[i].holders[uint(part)];
			bl_h[i] = history[i].blockNo;
		}

		return (list, bl_h);
	}

	// Function to get the done status of tuna
	function getDone() public view returns (bool[7]) {
		return current.done;
	}

	// Done function. Tuna's end goal is to be eaten by the consumer, and once that has happened, there is no reason to keep references to that tuna.
	// This function triggers a self-destruct once all the parts has been 'done', to save space on all EVMs.
	function done(Tuna_Parts part) onlyHolder public {
		require (msg.sender == current.holders[uint(part)], "Only the holder of the fish part can mark it as done");
		require (current.done[uint(part)] == false, "Only undone tuna can be marked as done");

		current.done[uint(part)] = true;
		bool suicide_ok = true; //check if contract can suicide
		for (uint i = 0; i < uint(Tuna_Parts.size); i++) {
			if (!current.done[i]) {
				break;
			}
		}

		if (suicide_ok) { //goodbye, world
			selfdestruct(owner);
		}
	}
}
