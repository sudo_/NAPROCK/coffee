# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os

wd = os.getcwd()
wd += "/VOCdevkit/VOC2012/JPEGImages"

print("The directory contains: ")
print(os.listdir(wd))

className = input("Enter class name: ")

i = 1

for file in os.listdir(wd):
    newName = wd + "/" + className + str(i) + ".jpg"
    curFile = wd + "/" + file
    try:
        os.rename(curFile,newName)
    except FileExistsError :
        print("A file of this name already exists!")
        print(newName)
    i += 1


print("Rename Complete! The directory now contains: ")
print(os.listdir(wd))
