from os import listdir
from os.path import isfile, join
open('trainval.txt', 'w').close()
mypath= "VOCdevkit/VOC2012/JPEGImages/"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
with open("VOCdevkit/VOC2012/ImageSets/Main/trainval.txt", "a") as f:
	f.truncate()
	for fi in onlyfiles:
		s=str(fi)
		f.write("%s\n" % (s[:-4]))
		print(s[:-4])
mypath= "VOCdevkit/VOC2007/JPEGImages/"
onlyfiles = [f for f in listdir(mypath) if isfile(join(mypath, f))]
with open("VOCdevkit/VOC2007/ImageSets/Main/test.txt", "a") as f:
	f.truncate()
	for fi in onlyfiles:
		s=str(fi)
		f.write("%s\n" % (s[:-4]))
		print(s[:-4])
