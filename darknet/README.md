# Using YoloV2 darknet implementation
------
Usually, you will need to make amendments to the MakeFile, change the value of CUDA, CUDNN to 1. 
This allows your computer to train and test using your GPU assuming you have one. Changing the value of OPENCV to 1 
allows for Real Time streaming on your computer with darknet. 
In our project we are not using this feature but a custom method to “stream”. 
However, it will not work as per intended with the stock executable by darknet. 

Enter the following commands to make the executable.
```
cd ~/darknet 

make
```

If error occurs, it means that CUDA is not installed properly. Refer to these links.

Follow this guide to install Cuda 9.0.176 and cuDNN 7:

https://yangcha.github.io/CUDA90/


and this guide to install Opencv 3.4.2.

https://milq.github.io/install-opencv-ubuntu-debian/



----


For labelling the images, we need to comply with the format used by darknet. For this, we used a VOCdevkit format and a labelling Image GUI.

https://github.com/tzutalin/labelImg/tree/master/

(Please refer here to see what dependencies are required for the GUI)

https://github.com/tzutalin/labelImg/tree/master/requirements

To install this tool:
```
git clone https://github.com/tzutalin/labelImg.git
cd ~/labelImg
python3 labelImg.py
```
Install dependencies via:
```
sudo apt-get install python3-pyqt5

sudo apt-get install lxml
```

-------

Below are some screenshots of our labelling process.

 ![](labelImg.jpg)
 
 
 ![](labelImg2.jpg)
 
 
                    Figure: labelling proccess
You can also view the labelling results by using the labelImg GUI. Open the folder VOCdevkit/VOC2012/JPEGImages and change save directory to VOCdevkit/VOC2012/Annotations of tuna_species or cross_section
