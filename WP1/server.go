package main

import (
	"crypto/rand"
	"fmt"
	"net/http"
	"log"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/gorilla/websocket"

	"./command_handler"
)

var upgrader = websocket.Upgrader{} //default upgrader options

// Serves whatever it can find in the local directory
func fileHandler(w http.ResponseWriter, req *http.Request) {
	path := "." + req.URL.Path
	log.Printf("%s requested path: %s\n", req.RemoteAddr, path)
	http.ServeFile(w, req, path)
}

// Upgrades the connection to WebSockets, and recieves an image file (.jpg)
func backendSendHandler(w http.ResponseWriter, req *http.Request) {
	c, err := upgrader.Upgrade(w, req, nil) //upgrade from HTTP to WS
	if err != nil {
		log.Println("Error while upgrading HTTP to WS connection: " , err)
		return
	}
	defer c.Close() //close after the whole function

	f, err := ioutil.ReadFile("noimage.jpg")
	if err != nil {
		log.Println("Error while opening 'none' image file: ", err)
		return
	}

	// Create a temporary folder for the current instance
	tmpdir, err := ioutil.TempDir("", "sudoCamera-")
	pendingPath := filepath.Join(tmpdir, "pending.jpg")
	log.Println("New temporary directory: ", tmpdir)
	log.Println("Corresponding pending: ", pendingPath)

	// Starts a DarknetProcessor
	dp := command_handler.NewProcessor(tmpdir, filepath.Join(tmpdir, "pending")) //pesky darknet appends its own jpg

	// Variable to store latest prediction
	var prediction string = ""

	// Deferments
	defer os.RemoveAll(tmpdir) //cleans up the temporary folder after done
	defer dp.End()

	for {
		// Client sends Image data.
		// Server sends latest pending image.
		_, image, err := c.ReadMessage() //reads the image
		if err != nil {
			log.Println("Error while obtaining image data from web socket: ", err)
			return
		}

		// Try to find the pending.jpg file
		if _, err := os.Stat(pendingPath); os.IsNotExist (err) { //didn't find it...
			err = c.WriteMessage(websocket.BinaryMessage, f)
			if err != nil {
				log.Println("Error while sending image data through web socket: ", err)
				return
			}
		} else { //found it
			pending, err := ioutil.ReadFile(pendingPath)
			if err != nil {
				log.Println("Error while opening the pending file.", err)
				return
			}

			err = c.WriteMessage(websocket.BinaryMessage, pending)
			if err != nil {
				log.Println("Error while sending image data through web socket: ", err)
				return
			}

			err = c.WriteMessage(websocket.TextMessage, []byte(prediction))
			if err != nil {
				log.Println("Error while sending prediction through web socket: ", err)
				return
			}
		}

		// Writes recieved data to file if DarknetProcessor is done
		select {
		case prediction = <-dp.Done:
			b_fileName := make([]byte, 16)
			_, err = rand.Read(b_fileName)
			if err != nil {
				log.Println("Unable to generate file name: ", err)
				return
			}
			fileName := filepath.Join(tmpdir, fmt.Sprintf("%x.jpg", b_fileName))
			log.Println(fileName) //debug line
			ioutil.WriteFile(fileName, image, 777)
			dp.Process(fileName)

		default:
			log.Println("Ignored.") //debug line
		}
	}
}

func main() {
	// Start HTTP(S) server
	http.HandleFunc("/backend/send", backendSendHandler)
	http.HandleFunc("/", fileHandler)
	err := http.ListenAndServeTLS(":8080", "server.crt", "server.key", nil)
	if err != nil {
		log.Fatal("Error while trying to start the server: ", err)
	}
}
