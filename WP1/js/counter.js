var count = 0;
var predictions = [];
document.getElementById("add-btn").onclick = function () {
    count++;
    predictions.push(document.getElementById("prediction").innerText);
    return true;
} //adds to the count

document.getElementById("sub-btn").onclick = function () {
    var form = document.createElement("form");
    form.method = 'post';
    form.action = "https://" + window.location.host.split(':')[0] + ":8081";
    form.hidden = true;
    
    var json = JSON.stringify({length: count, data: predictions});
    var input = document.createElement("input");
    input.name = "json";
    input.value = json;
    form.append(input);

    document.getElementsByTagName("body")[0].append(form);
    form.submit();

    return false;
} //submit button

