# Web Page 1 (./WP1) - Advanced
Although not demonstrated in the basic section, the authors recommend the Amazon Machine Images of ami-09f0be2e7a739216e or ami-0ebf60d991db0c2c8 when launching the EC2 instances in place of the ami-059eeca93cf09eebd (an Ubuntu 16.04 AMI), because most of the tedious work of setting up is alleviated.

As mentioned in the basic section, the instance type of p3.16xlarge is recommended for the production use of WP1, but any instance can be chosen if a minor change is done to the Makefile accordingly.
If the operator is using any GPU-capable instance, then a change is not required.
If the operator is not using any GPU-capable instance, then cuda, cuDNN, and OpenCV no longer needs to be installed. The operator will need to replace the top in the contents of the Makefile with:
```
GPU=0
CUDNN=0
OPENCV=0
```
Then, the operator can proceed on with the remaining of the basic section. Please note that running the WP1 server with just the CPU will cause it to be exorbitantly slow (very taxing on the hardware) when serving live streaming content.
