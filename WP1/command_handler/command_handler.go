// Each websocket should be tied with one command handler
package command_handler

import (
	"bufio"
	"io"
	"log"
	"os"
	"os/exec"
	"strings"
	"sync"
)

const darknetCommand string = "../darknet/darknet"
const darknetData string = "../darknet/yolov2_tuna_species/tuna.data"
const darknetCfg = "../darknet/yolov2_tuna_species/yolov2_tunaspecies.cfg"
const darknetWeights = "../darknet/yolov2_tuna_species/yolov2_tunaspecies_8000.weights"

// The darknet processor. Done is signalled when the processor finishes
type DarknetProcessor struct {
	Done chan string
	cmd *exec.Cmd
	stdout, stderr *io.ReadCloser
	stdin *io.WriteCloser
	dir, file, processing string
	mutex *sync.Mutex
}

// Creates a new darknet processor (dir = temporary directory, file = finalized output file path)
func NewProcessor(dir, file string) (dp *DarknetProcessor) {
	// Create the command object
	cmd := exec.Command(darknetCommand, "detector", "test", darknetData, darknetCfg, darknetWeights, "-out", file)
	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()
	stdin, _ := cmd.StdinPipe()

	// Starts the command
	err := cmd.Start()
	if err != nil {
		log.Printf("Error occured while creating a new Darknet processor: %s", err)
		return nil
	}

	// Creates the DarknetProcessor struct object
	dp = &DarknetProcessor{
		make(chan string),
		cmd,
		&stdout, &stderr,
		&stdin,
		dir,
		file,
		"",
		new(sync.Mutex),
	}

	// Starts the done listener
	go dp.doneListener()
	return //returns dp
}

// Function to signal to the channel when an image has been fully processed
func (dp *DarknetProcessor) doneListener() {
	dp.Done <- "" // when the listener starts, signal a done
	reader := bufio.NewReader(*dp.stdout)

	for { //infinite loop until stream reaches EOF (should only happen when it closes)
		output, err := reader.ReadString('\n')
		if strings.Contains(output, "Predicted") {
			dp.mutex.Lock()
			//Delete processed file
			err = os.Remove(dp.processing)
			if err != nil {
				log.Println("Error occured while trying to delete temporary file: ", err)
			}
			dp.mutex.Unlock()

			if peeked, err := reader.Peek(1); err == nil && (peeked[0] == 'y' || peeked[0] == 'b' || peeked[0] == 's' || peeked[0] == 'a') {
				output, err = reader.ReadString('\n') //read another line
				dp.Done <- output //signal done, with prediction output
			} else {
				dp.Done <- "" //signal done only
			}
		}

		if err == io.EOF {
			break
		} else if err != nil {
			log.Println("Error occured while attempting to listen to stdout for done messages: ", err)
			return
		}
		log.Print(output)
	}
}

// Processes an input (file is a path to an existing image file to process)
func (dp *DarknetProcessor) Process(file string) {
	io.WriteString(*dp.stdin, file + "\n") // Writes to the input of the command
	dp.mutex.Lock()
	dp.processing = file
	dp.mutex.Unlock()
}

// Function to terminate the process
func (dp *DarknetProcessor) End() {
	log.Println("Ending darknet processor...")
	if err := dp.cmd.Process.Kill(); err != nil {
		log.Fatal("Cannot end child process: ", err)
	}
	dp.cmd.Wait()
	log.Println("Ended darknet processor.")
}
