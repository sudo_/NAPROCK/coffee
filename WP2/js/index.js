var addBtn = document.getElementById("add-btn");
var stateLabel = document.getElementById("state-label");
var qualityLabel = document.getElementById("quality-label");
var predictionLabel = document.getElementById("prediction");
var fishCount = 0;
var badCount = 0;
var submitForm = document.getElementById("submit-form");
var data = document.getElementById("data");

const BAD_COUNT_THRESHOLD = 2; //decided by fair dice roll.
var SUBMISSION_IP = ""; //the submission IP. If empty, will default to current hostname:6000

// Add button event listener
addBtn.addEventListener("click", () => {
    if (predictionLabel.innerText == "") { //ensures that there is a prediction
	qualityLabel.innerText = "Can't see tuna.";
	return;
    }
    
    fishCount += 1;
    // Update state label
    if (fishCount < 10)
	stateLabel.innerText = "Processing... (" + fishCount + "/10)";
    else stateLabel.innerText = "Done processing. Determining results...";

    // Display the quality of the previous fish
    var quality = predictionLabel.innerText.split(':')[0];
    qualityLabel.innerText = quality;

    // Add to badCount if prediction is akami
    if (quality == "akami")
	badCount += 1;

    // Determine state if fishCount is at 10.
    if (fishCount >= 10) {
	if (badCount >= BAD_COUNT_THRESHOLD)
	    stateLabel.innerText = "Release the batch.";
	else {
	    stateLabel.innerText = "Batch is good. The submission form should spawn now.";
	    addBtn.disabled = true; //no don't click add anymore please

	    if (SUBMISSION_IP == "") //set the submission IP if the programmer didn't do that
		SUBMISSION_IP = document.location.hostname.split(':')[0] + ":6001";

	    // Populate the data field
	    var cookies = document.cookie.split(";");
	    var json_data = null;

	    for (var i = 0; i < cookies.length; i++) {
		var cookie = cookies[i].split("=");
		var cookie_name = cookie[0];
		var cookie_key = cookie[1];

		if (cookie_name === "json") {
		    jsonData = cookie_key;
		    break;
		}
	    }

	    if (jsonData == null) return; //don't precceed if json data is not found

	    data.value = jsonData;
	    
            submitForm.action = "https://" + SUBMISSION_IP;
	    submitForm.style = ""; //appearize the form
	}
    }
});
