const relativeDataServerURL = "wss://" + window.location.host + "/backend/send"; //the relative url to submit the video data

// Define the constraints used to obtain the user's cameras
var constraints = {
    audio: false,
    video: true
};

// Define variables used throughout the script
var width = 608;
var height = 608;
var video = document.getElementById("webcamStream");
var canvas = document.getElementById("capturedFrame");
var photo = document.getElementById("outputBox");
var label = document.getElementById("prediction");

// Attempts to get the stream for the user's camera
navigator.mediaDevices.getUserMedia(constraints)
    .then((stream) => {
	video.srcObject = stream;
	video.play();
    })
    .catch((err) => {
	alert("Denied access to camera! Maybe it doesn't exist?")
	console.log("Error occured while attempting to get a media device: " + err);
    });

// Event listener to patiently wait for the video to start playing
video.addEventListener('canplay', (ev) => {
    captureFrameAndUpload(); //runs the frame capture indefinitely
});

// Function that generates a promise that does nothing but times out
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

// Async function to capture a single frame from the video tag, and then waits for a certain interval
async function captureFrameAndUpload() {
    while (true) {
	takePicture();
	await sleep(5);
    }
}

// Function to clear the photo in the <img> tag
function clearPhoto() {
    var context = canvas.getContext('2d');
    context.fillStyle = "#AAA"; //light grey
    context.fillRect(0, 0, canvas.width, canvas.height);

    var data = canvas.toDataURL('image/jpg');
    photo.setAttribute('src', data);
}

// Fuction to take a picture and sends it to the backend server
function takePicture() {
    var context = canvas.getContext('2d');
    context.drawImage(video, 0, 0, width, height); //takes a still frame

    var data = canvas.toDataURL('image/jpg');
    fetch(data).then(res => res.blob()).then(blob => sendFrame(blob)) //also sends the frame to the server
}

var ws = new WebSocket(relativeDataServerURL);
var loaded_image = null;
ws.onopen = (evt) => {
    console.log("WebSocket open.");
};

ws.onclose = (evt) => {
    console.log("WebSocket closed.");
};

ws.onmessage = (evt) => {
//    console.log("Response recieved. Now displaying...");
//    console.log(typeof evt.data);
    if (typeof evt.data === "object") {
	if (loaded_image != null)
	    URL.revokeObjectURL(loaded_image);

	loaded_image = URL.createObjectURL(evt.data);
	photo.setAttribute('src', loaded_image);
    }

    console.log(evt.data);
    
    if (typeof evt.data === "string")
	if (evt.data != "")
	    label.innerText = evt.data;
        else label.innerText = "Nothing detected.";
};

ws.onerror = (evt) => {
    console.error("Error: " + evt.data);
};

// Function to make a POST request to the backend server
function sendFrame(data) {
    ws.send(data);
}
