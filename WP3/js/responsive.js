document.getElementById("submitter").onclick = function() {
    //Declare variables to be carried
    var account = document.getElementsByName("account")[0];
    var fishid = document.getElementsByName("fishid")[0];
    var fishpart = document.getElementsByName("fishpart")[0];

    var formData = {"account": account.value, "fishid": fishid.value, "fishpart": fishpart.value};
       
    //Declare initialization of the request
    var headers = new Headers();
    headers.append('Content-Type', 'application/json');
    var init = {
	method: "POST",
	headers: headers,
	body: JSON.stringify(formData)
    };
    var request = new Request("/validate", init);
    
    //Fetch the request
    fetch(request).then(function(response) {
	document.getElementById("queryBox").hidden = true;
	var validatedText = document.getElementById("validatedText");
	response.text().then(function (text) {
	    if (text == "true") {
		validatedText.innerText = "Validated!";
		validatedText.style.color = "lime";
	    } else {
		validatedText.innerText = "Not genuine :(";
		validatedText.style.color = "red";
	    }
	});
    });
}
