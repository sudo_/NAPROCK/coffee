// Express-based app to:
// 1) Serve pages as required
// 2) Takes in 3 data inputs on POST, namely: Restaurant/Vendor ID, Fish ID, Fish Part, and returns true/false if parameters are correct
var express = require('express');
var app = express();

const fs = require('fs')
const Web3 = require('web3')
const web3 = new Web3("http://localhost:8545")

// Body parser
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Load tuna contract
var content = JSON.parse(fs.readFileSync("tuna.js", {encoding: "utf-8"}).split('=')[1]);
const abi = content.contracts["tuna.sol:Tuna_Sale"].abi;
const contract = new web3.eth.Contract(JSON.parse(abi));

// Basic all allow file server
app.get('/*', (req, res) => {
    var options = {
        root: __dirname,
        headers: {
            'x-timestamp': Date.now(),
            'x-sent': true
        }
    };

    var fileName = req.params[0];
    if (fileName == "") fileName = "index.html";
    res.sendFile(fileName, options, (err) => {
        if (err) {
            console.log("Error while sending a file: ", err);
            res.sendStatus(500);
        } else {
            console.log(req.ip, ": ", fileName, " sent.");
        }
    });
});

// POST and return
app.post('/validate', (req, res) => {
        // Check if the individual ids are passed into POST
    if (!("account" in req.body)) {
	console.log("(Account Param) Insufficient parameters when validating for: ", req.ip);
	res.sendStatus(500);
	return;
    }

    if (!("fishid" in req.body)) {
	console.log("(FishID Param) Insufficient parameters when validating for: ", req.ip);
	res.sendStatus(500);
	return;
    }

    if (!("fishpart" in req.body)) {
	console.log("(FishPart Param) Insufficient paramters when validating for: ", req.ip);
	res.sendStatus(500);
	return;
    }

    // Communicate to blockchain to figure out if this is a skamas
    var current_contract = contract.clone();
    current_contract.options.address = req.body.fishid;
    
    current_contract.methods.getTunaHolder(req.body.fishpart.toLowerCase().charCodeAt(0) - 'a'.charCodeAt(0)).call((err, result) => {
	if (result[0].toLowerCase() == req.body.account.toLowerCase())
	    res.send("true");
	else res.send("false");
    });
});

app.listen(8082);
